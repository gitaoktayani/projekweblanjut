@extends('layout')
@section('page')
    About Me
@endsection
@section('jenis')
    
<body class="about">
@endsection
@section('konten')
    

<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>ABOUT <span>ME</span></h1>
    <span class="title-bg">Resume</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
        <div class="row">
            <!-- Personal Info Starts -->
            <div class="col-12 col-lg-10 col-xl-6">
                <div class="row">
                    <div class="col-12">
                        <h3 class="text-uppercase custom-title mb-0 ft-wt-600">personal info</h3>
                    </div>
                    <div class="col-12 d-block d-sm-none">
                        <img src="img/img-mobile.jpg" class="img-fluid main-img-mobile" alt="my picture" />
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">full name :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Ni Putu Gita Oktayani</span> </li>
                            <li> <span class="title">Age :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">19 Years</span> </li>
                            <li> <span class="title">Nationality :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia</span> </li>
                            <li> <span class="title">Proffesion :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Student</span> </li>
                        </ul>
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">Birth :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Gelgel, 21 Oktober 2001</span> </li>
                            <li> <span class="title">Address :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Klungkung,Bali,Indonesia</span> </li>
                            <li> <span class="title">phone :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">085976440637</span> </li>
                            <li> <span class="title">Email :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">gitaoktayani@gmail.com</span> </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Personal Info Ends -->
                    <hr class="separator mt-1">
        <!-- Experience & Education Starts -->
        <div class="row">
            <div class="col-12">
                <h3 class="text-uppercase pb-5 mb-0 text-left text-sm-center custom-title ft-wt-600">Experience <span>&</span> Education</h3>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fa fa-briefcase"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2019</span>
                            <h5 class="poppins-font text-uppercase">Tenaga Kerja Situng 2019<span class="place open-sans-font">KPU KLUNGKUNG</span></h5>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-briefcase"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2018</span>
                            <h5 class="poppins-font text-uppercase">Professional Placement <span class="place open-sans-font">Ganeshcom Studio</span></h5>
                        </li>
                        
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2019-Present</span>
                            <h5 class="poppins-font text-uppercase">Informatics Engineering <span class="place open-sans-font">Ganesha University of Education</span></h5>
                            
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2016-2019</span>
                            <h5 class="poppins-font text-uppercase">VocationaL High School <span class="place open-sans-font">SMK TI BALI GLOBAL KLUNGKUNG</span></h5>
                           
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2013-2016</span>
                            <h5 class="poppins-font text-uppercase">Junior High School <span class="place open-sans-font">SMP N 1 SEMARAPURA</span></h5>
                            
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2007-2013</span>
                            <h5 class="poppins-font text-uppercase">Elementary School <span class="place open-sans-font">SD N 1 GELGEL</span></h5>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Experience & Education Ends -->
    </div>
</section>
@endsection