<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\ViewController::class,'index']);

Route::get('/about', [App\Http\Controllers\ViewController::class,'about']);

Route::get('/contact',[App\Http\Controllers\ViewController::class,'contact']);
