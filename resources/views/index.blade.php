﻿@extends('layout')
@section('page')
    Personal Profil
@endsection
@section('jenis')
    
<body class="home">
@endsection
@section('konten')
    

<section class="container-fluid main-container container-home p-0 revealator-slideup revealator-once revealator-delay1">
    <div class="color-block d-none d-lg-block"></div>
    <div class="row home-details-container align-items-center">
        <div class="col-lg-4 bg position-fixed d-none d-lg-block"></div>
        <div class="col-12 col-lg-8 offset-lg-4 home-details text-left text-sm-center text-lg-left">
            <div>
                <img src="/public/assets/gambar/gita.jpg" class="img-fluid main-img-gita d-none d-sm-block d-lg-none" alt="my picture" />
                <h6 class="text-uppercase open-sans-font mb-0 d-block d-sm-none d-lg-block">hi there !</h6>
                <h1 class="text-uppercase poppins-font"><span>I'm</span> Gita Oktayani</h1>
                <p class="open-sans-font">I'm a student in Informatics Engineering, Faculty of Engineering and Vocational, Ganesha University of Education </p>
                <a href="/about" class="btn btn-about">more about me</a>
            </div>
        </div>
    </div>
</section>
@endsection
<!-- Main Content Ends -->

