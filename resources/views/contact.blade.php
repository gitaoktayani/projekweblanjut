@extends('layout')
@section('page')
    Contact
@endsection
@section('jenis')
    
<body class="contact">
@endsection
@section('konten')
    

<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>get in <span>touch</span></h1>
    <span class="title-bg">contact</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
        <div class="row">
            <!-- Left Side Starts -->
            <div class="col-12 col-lg-100">
                <h3 class="text-uppercase custom-title mb-0 ft-wt-600 pb-3">let's get closer</h3>
                <p class="open-sans-font mb-3">you can get closer to me by contacting the contact below</p>
                <p class="open-sans-font custom-span-contact position-relative">
                    <i class="fa fa-envelope-open"></i>
                    <span class="d-block" >mail me</span>gitaoktayani@gmail.com
                </p>
                <p class="open-sans-font custom-span-contact position-relative">
                    <i class="fa fa-phone-square "></i>
                    <span class="d-block">call me</span>085976440637
                </p>
                <ul class="social list-unstyled pt-1 mb-5" >
                    <li class="facebook"><a title="Facebook" href="https://www.facebook.com/profile.php?id=100007726366764"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="youtube"><a title="Youtube" href="https://www.youtube.com/channel/UCdmdvc87NIj4nQtHwQqPXQg"><i class="fa fa-youtube"></i></a>
                    </li>
                    <li class="whatsapp"><a title="Instagram" href="https://www.instagram.com/gitaoktayani/?hl=en"><i class="fa fa-instagram"></i></a>
                    </li>
                    <li class="whatsapp"><a title="Whatsapp" href="https://wa.me/6285976440637"><i class="fa fa-whatsapp"></i></a>
                    </li>
                </ul>
            </div>
            <!-- Left Side Ends -->
           
        </div>
    </div>

</section>
@endsection
